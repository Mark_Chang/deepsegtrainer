import json
from collections import OrderedDict
from util import read_json, write_json


class Network(object):
    def __init__(self, config, datagen):
        self.config = config
        self.input = OrderedDict()
        self.wb = OrderedDict()
        self.gradients_sq = OrderedDict()
        self.deltas_sq = OrderedDict()
        self.datagen = datagen
        self.params = {
            'wb': self.wb,
            'gradients_sq': self.gradients_sq,
            'deltas_sq': self.deltas_sq,
        }

    def write_model(self, fname, f_indent=0):
        params = self.params
        params_json = {}
        p_keys = ['wb', 'gradients_sq', 'deltas_sq']
        for p_key in p_keys:
            params_json[p_key] = {}
            for w_key in params[p_key]:
                params_json[p_key][w_key] = params[p_key][w_key].get_value().tolist()
        params_json['params'] = {}
        if params.get('params'):
            params_json['params'] = json.dumps(params['params'])
        write_json(fname, params_json, indent=f_indent)

    def load_model(self, fname):
        params_json = read_json(fname)
        params = self.params
        p_keys = ['wb', 'gradients_sq', 'deltas_sq']
        for p_key in p_keys:
            for w_key in params[p_key]:
                if w_key in params_json[p_key]:
                    params[p_key][w_key].set_value(params_json[p_key][w_key])

    def t_func(self, *args):
        pass

    def v_func(self, *args):
        pass

    def run_t_func(self):
        for input_data in self.datagen.gen_input_data(is_train=True):
            yield self.t_func(*input_data)

    def run_v_func(self):
        for input_data in self.datagen.gen_input_data(is_train=False):
            yield self.v_func(*input_data)
