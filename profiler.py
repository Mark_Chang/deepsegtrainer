import time


class Profiler(object):
    def __init__(self, name=""):
        self._total = 0
        self._period = 0
        self._name = name
        self._current = 0

    def start(self):
        self._current = time.time()

    def stop(self):
        self._period = time.time() - self._current
        self._total += self._period

    def reset(self):
        self._total = 0
        self._current = 0

    def __str__(self):
        return "%s total: %s" % (self._name, self._total)

    def get_period(self):
        return "%s period: %s" % (self._name, self._period)

    def get_total(self):
        return "%s total: %s" % (self._name, self._total)


class AvgCounter(object):
    def __init__(self, name=""):
        self._count = 0
        self._value = 0
        self._name = name

    def add(self, val):
        self._count += 1
        self._value += val

    def reset(self):
        self._count = 0
        self._value = 0

    def get_and_reset(self):
        temp_count = self._count
        temp_val = self._value
        self.reset()
        if temp_count == 0:
            return 0
        else:
            return temp_val / float(temp_count)

    def get_and_reset_str(self):
        return "%s : %s" % (self._name, self.get_and_reset())


def main():
    p = Profiler("test")
    p.start()
    time.sleep(0.5)
    p.stop()
    print p.get_period()
    print p.get_total()
    p.start()
    time.sleep(0.5)
    p.stop()
    print p.get_period()
    print p.get_total()

    a = AvgCounter("test")
    a.add(3)
    a.add(5)
    print a.get_and_reset_str()
    a.add(2)
    a.add(9)
    print a.get_and_reset_str()


if __name__ == "__main__":
    main()
