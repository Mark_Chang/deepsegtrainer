import theano
import numpy as np


def weight_init( shape):
    return theano.shared(
        ((0.5 - np.random.rand(*shape)) * (np.sqrt(6) / np.sqrt(sum(shape)))).astype(theano.config.floatX))

def bias_init( shape):
    return theano.shared(np.zeros(shape).astype(theano.config.floatX))

def weight_init_val( w_val):
    return theano.shared(np.array(w_val).astype(theano.config.floatX))

def weight_init_const( shape, const):
    return theano.shared((const * np.ones(shape)).astype(theano.config.floatX))

