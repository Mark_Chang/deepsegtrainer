# -*- encoding:utf-8 -*-
import json
import re

NUM_LIST = [
    u"０", u"１", u"２", u"３", u"４", u"５", u"６", u"７", u"８", u"９"
]

ENG_LIST = [
    u"Ａ", u"Ｂ", u"Ｃ", u"Ｄ", u"Ｅ", u"Ｆ", u"Ｇ", u"Ｈ", u"Ｉ", u"Ｊ",
    u"Ｋ", u"Ｌ", u"Ｍ", u"Ｎ", u"Ｏ", u"Ｐ", u"Ｑ", u"Ｒ", u"Ｓ", u"Ｔ",
    u"Ｕ", u"Ｖ", u"Ｗ", u"Ｘ", u"Ｙ", u"Ｚ", u"ａ", u"ｂ", u"ｃ", u"ｄ",
    u"ｅ", u"ｆ", u"ｇ", u"ｈ", u"ｉ", u"ｊ", u"ｋ", u"ｌ", u"ｍ", u"ｎ",
    u"ｏ", u"ｐ", u"ｑ", u"ｒ", u"ｓ", u"ｔ", u"ｕ", u"ｖ", u"ｗ", u"ｘ",
    u"ｙ", u"ｚ"
]


def read_file_line(fname, toprint=False):
    f = open(fname, "r")
    i = 0
    while True:
        line = f.readline()
        i += 1
        if toprint:
            print i
        if len(line) == 0:
            break
        yield line
    f.close()


def read_json(json_fname):
    f = open(json_fname, "r")
    json_item = json.loads("".join(f.readlines()))
    f.close()
    return json_item


def write_json(json_fname, json_item, indent=0):
    f = open(json_fname, "w")
    f.write(json.dumps(json_item, indent=indent))
    f.close()


def tagDigitEn(w):
    if w.isdigit() or w in NUM_LIST:
        return u"$NUM$"
    elif len(re.findall(ur"[A-Za-z]", w)) > 0 or w in ENG_LIST:
        return u"$EN$"
    else:
        return w

def get_widx(w, word_dict):
    w_idx = word_dict.get(tagDigitEn(w), -1)
    if w_idx != -1:
        return w_idx
    else:
        return word_dict.get("OOV")


def gen_word_idx(line, word_dict):
    line_write = []
    line2 = filter(lambda x: len(x) > 0, [w.strip() for w in line])
    if len("".join(line2)) > 0:
        for w in line2:
            w_idx = get_widx(w, word_dict)
            line_write.append(w_idx)
    return line_write


def gen_word_label(line, word_dict):
    line_write = []
    line2 = filter(lambda x: len(x) > 0, [w.strip() for w in line.split(" ")])
    if len("".join(line2)) > 0:
        for ws in line2:
            if len(ws) == 1:
                line_write.append("%s,%s" % (get_widx(ws, word_dict), 0))
            else:
                for i, w in enumerate(ws):
                    if i == 0:
                        line_write.append("%s,%s" % (get_widx(w, word_dict), 1))
                    elif i == len(ws) - 1:
                        line_write.append("%s,%s" % (get_widx(w, word_dict), 2))
                    else:
                        line_write.append("%s,%s" % (get_widx(w, word_dict), 3))
    return line_write
