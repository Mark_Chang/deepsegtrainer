import numpy as np
import math
import copy
from util import read_file_line, read_json, write_json
import my_logger


class DataGenerator(object):
    def __init__(self, params):
        self.pad_id = params["pad_id"]
        self.s_window = params["s_window"]
        self.train_file_idx = params["train_file_idx"]
        self.valid_file_idx = params.get("valid_file_idx","")
        self.random_preload = params.get("random_preload", True)
        self.my_print = my_logger.my_print

    def read_input_file(self, is_train=True):
        if is_train or len(self.valid_file_idx) > 0:
            file_to_read = self.train_file_idx
        else:
            file_to_read = self.valid_file_idx
        lines = [line for line in read_file_line(file_to_read, False)]
        if self.random_preload:
            np.random.shuffle(lines)
        for line in lines:
            if len(line.strip()) > 0:
                yield line

    def gen_input_line(self, line):
        pad_id = self.pad_id
        s_window = self.s_window
        line_word = [pad_id] * (s_window / 2) + [int(w.split(",")[0]) for w in line.strip().split(" ")] + [pad_id] * (
            s_window / 2)
        case_raw = [line_word[i:i + s_window] for i in range(len(line_word) + 1 - s_window)]
        line_tag = [int(w.split(",")[1]) for w in line.strip().split(" ")]

        return case_raw, line_tag

    def gen_input_data(self, is_train=True):
        for line in self.read_input_file(is_train):
            case_x, case_y = self.gen_input_line(line)
            yield case_x, case_y


class SeqDataGenerator(DataGenerator):
    def __init__(self, params):
        super(SeqDataGenerator, self).__init__(params)
        self.input_size = params.get("input_size", 10000)
        self.remaining_size = params.get("remaining_size", 100)

    def gen_input_data(self, is_train=True):
        case_x_out = []
        case_y_out = []
        for line in self.read_input_file(is_train):
            case_x, case_y = self.gen_input_line(line)
            case_x_out.extend(case_x)
            case_y_out.extend(case_y)
            if len(case_x_out) >= self.input_size:
                yield np.array(case_x_out), np.expand_dims(np.array(case_y_out), axis=1)
                case_x_out = []
                case_y_out = []
        msg = "remaining: %s " % (len(case_x_out))
        self.my_print(msg)
        if len(case_x_out) >= self.remaining_size:
            yield np.array(case_x_out), np.expand_dims(np.array(case_y_out), axis=1)


class SegNegSampDataGenerator(SeqDataGenerator):
    def __init__(self, params):
        super(SegNegSampDataGenerator, self).__init__(params)
        self.s_negsamp = params.get("s_negsamp", params["s_vocab"])

    def set_nagsamp_size(self, s):
        self.s_negsamp = s

    def gen_neg_idx(self, target):
        while True:
            # result = #negsamp_array[int(math.floor(np.random.rand() * len(negsamp_array)))]
            result = int(math.floor(np.random.rand() * (self.s_negsamp)))
            if result != target:
                return result

    def gen_input_line(self, line):
        line_word = [self.pad_id] * (self.s_window / 2) + [int(w.split(",")[0]) for w in line.strip().split(" ")] \
                    + [self.pad_id] * (self.s_window / 2)
        case_raw = [line_word[i:i + self.s_window] for i in range(len(line_word) + 1 - self.s_window)]
        case_positive_raw = zip(case_raw, [1] * len(case_raw))
        case_negative_raw = zip(
            [copy.deepcopy(item[:self.s_window / 2]) + [self.gen_neg_idx(item[self.s_window / 2])] + copy.deepcopy(
                item[self.s_window / 2 + 1:])
             for item in case_raw], [0] * len(case_raw))
        case_all = case_positive_raw + case_negative_raw
        np.random.shuffle(case_all)
        [case_x, case_y] = zip(*case_all)
        case_x = list(case_x)
        case_y = list(case_y)
        return case_x, case_y


def main():
    params = {
        "pad_id": 3999,
        "s_window": 5,
        "train_file_idx": "./data/converted/pku_valid_c.utf8",
        "valid_file_idx": "./data/converted/pku_valid_c.utf8",
        "batch_size": 8,
        "truncated_len": 300,
    }
    data_gen = DataGenerator(params)
    for x, y in data_gen.gen_input_data():
        write_json("example_xy.json", {"x": x, "y": y}, indent=4)
        #    break
        #    print 1


if __name__ == "__main__":
    main()
