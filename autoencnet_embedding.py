import theano.tensor as T
from collections import OrderedDict
from autoencnet import AutoEncNetwork


class AutoEncNetworkEmbedding(AutoEncNetwork):
    def __init__(self, config, datagen=None):
        super(AutoEncNetworkEmbedding, self).__init__(config, datagen)
        self.s_e_hidden = config["s_e_hidden"]
        self.s_out = 1
        self.build_networks()

    def build_wb_wrapper(self):
        wb_shape = OrderedDict({
            "w_embed": (self.s_vocab, self.s_embed),
            "w_e_hidden": (self.s_embed * self.s_window, self.s_e_hidden),
            "b_e_hidden": (self.s_e_hidden,),
            "w_out": (self.s_e_hidden, self.s_out),
            "b_out": (self.s_out,),

        })
        return wb_shape

    def build_hidden_layer(self, hidden_input):
        hidden_result = T.tanh(T.dot(hidden_input, self.wb["w_e_hidden"]) + self.wb["b_e_hidden"])
        return hidden_result




if __name__ == "__main__":
    print 1
