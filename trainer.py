import numpy as np
from profiler import Profiler, AvgCounter
import my_logger


def get_correct_ratio(golden, predicted):
    assert len(golden) == len(predicted)
    return np.sum(golden == (predicted > 0.5).astype(int)) / float(len(golden))


class Trainer(object):
    def __init__(self, network):
        self.my_print = my_logger.my_print
        self.network = network
        # self.datagen = datagen
        pass

    def trainig(self, nconfig):
        model_name_final = ""
        logfile = nconfig.get("logfile", "out.log")
        if len(nconfig.get("valid_file_idx", "")) > 0:
            do_valid = True
        else:
            do_valid = False
        my_logger.set_logfile(logfile=logfile)
        tprofiler = Profiler("train")
        avg_t_cost = AvgCounter("cost")
        avg_t_error = AvgCounter("error")

        avg_v_error_all = AvgCounter("valid error all")

        avg_t_error_all = AvgCounter("train error all")

        avg_error_cost = AvgCounter("error cost")
        avg_reg_cost = AvgCounter("reg cost")

        model_name = nconfig.get("model_name", "model_%s.json")
        model_count = nconfig.get("model_offset", 0)
        print_time = nconfig.get("print_time", 1000)
        wait_time = nconfig.get("valid_wait_time", 30)
        train_iter = nconfig.get("iter", 400)

        min_valid_error = np.inf
        valid_wait = 0
        for iter in range(train_iter):
            tprofiler.start()
            j = 0
            # for line in read_input_file(train_file_idx, False):
            for result in self.network.run_t_func():

                j += 1

                avg_t_cost.add(result[0])
                avg_t_error.add(result[1])
                avg_t_error_all.add(result[1])
                avg_error_cost.add(result[-2])
                avg_reg_cost.add(result[-1])

                if j % print_time == 0:
                    msg = "iter=%s, j=%s, %s ,%s" % (
                        iter, j,
                        avg_t_cost.get_and_reset_str(),
                        avg_t_error.get_and_reset_str()
                    )
                    self.my_print(msg)

                    msg2 = "%s, %s" % (
                        avg_error_cost.get_and_reset_str(),
                        avg_reg_cost.get_and_reset_str()
                    )
                    self.my_print(msg2)

            self.my_print("do validation...")

            avg_t_error_val = avg_t_error_all.get_and_reset()

            if do_valid:
                for result in self.network.run_v_func():
                    avg_v_error_all.add(result[1])

                avg_v_error_val = avg_v_error_all.get_and_reset()

                msg = "avg v error : %s" % (avg_v_error_val)
                msg += ", avg t error : %s" % (avg_t_error_val)
            else:
                avg_v_error_val = avg_t_error_val
                msg = "avg t error : %s" % (avg_t_error_val)

            if avg_v_error_val < min_valid_error:
                valid_wait = 0
                min_valid_error = avg_v_error_val
                model_name_save = model_name % model_count
                model_count += 1
                self.network.write_model(model_name_save)
                msg += ", save_model=%s" % (model_name_save)
                model_name_final = model_name_save
            else:
                valid_wait += 1
                msg += ", valid_wait=%s" % (valid_wait)
                if valid_wait > wait_time:
                    msg += ", early stop"
                    self.my_print(msg)
                    break

            self.my_print(msg)
            tprofiler.stop()
            self.my_print(tprofiler.get_period())
        return model_name_final


if __name__ == "__main__":
    pass
