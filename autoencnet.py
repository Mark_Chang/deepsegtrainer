import theano.tensor as T
import theano
import numpy as np
from collections import OrderedDict
from util import read_file_line, read_json, write_json
from network import Network
import initializer
import optimizer


class AutoEncNetwork(Network):
    def __init__(self, config, datagen=None):
        super(AutoEncNetwork, self).__init__(config, datagen)
        self.s_vocab = config["s_vocab"]
        self.s_embed = config["s_embed"]
        self.s_window = config["s_window"]
        self.p_lambda = config.get("p_lambda", 0.0001)
        self.p_mloss = config.get("p_mloss", 0.2)
        self.p_rho = self.config.get("p_rho", 0.95)
        self.p_lr = self.config.get("p_lr", 0.1)
        self.p_eps = self.config.get("p_eps", 0)
        self.p_optimizer = self.config.get("p_optimizer", "clipped_adagrad")
        self.is_train = self.config.get("is_train", True)
        self.s_out = 4

    def build_wb_wrapper(self):
        wb_shape = OrderedDict({
            "w_embed": (self.s_vocab, self.s_embed),
            "w_out": (self.s_embed * self.s_window, self.s_out),
            "b_out": (self.s_out,),
        })
        return wb_shape

    def build_input(self):
        self.input["x_in"] = T.imatrix('x_in')
        self.input["y_in"] = T.imatrix('y_in')

    def build_wb(self, wb_shape):
        for wb_s_key in wb_shape.keys():
            if wb_s_key[0] == "w":
                self.wb[wb_s_key] = initializer.weight_init(wb_shape[wb_s_key])
            else:
                self.wb[wb_s_key] = initializer.bias_init(wb_shape[wb_s_key])

    def y_seq_func(self, x_in):
        results = self.func_y_seq(x_in)
        return results

    def t_func(self, x_in, y_gold):
        result = self.func_test(x_in)
        return self.func_trainer(x_in, y_gold)

    def v_func(self, x_in, y_gold):
        return self.func_validator(x_in, y_gold)

    def load_pre_model(self, fname):
        params_json = read_json(fname)
        params = self.params
        for w_key in params['wb']:
            if w_key not in ['w_out', 'b_out']:
                if w_key in params_json['wb']:
                    params['wb'][w_key].set_value(params_json['wb'][w_key])

    def build_hidden_layer(self, hidden_input):
        return hidden_input

    def build_networks(self):
        self.build_input()
        self.build_wb(self.build_wb_wrapper())

        x_in_1dim = T.reshape(self.input["x_in"], newshape=(self.input["x_in"].shape[0] * self.s_window,))
        emb_lookup = self.wb["w_embed"][x_in_1dim]

        hidden_input = T.reshape(emb_lookup,
                                 newshape=(self.input["x_in"].shape[0], self.s_embed * self.s_window))

        hidden_result = self.build_hidden_layer(hidden_input)

        off = 1e-6
        if self.s_out == 1:
            out_result = T.nnet.sigmoid(T.dot(hidden_result, self.wb["w_out"]) + self.wb["b_out"])
            out_result_tag = T.cast(T.ge(out_result, 0.5), theano.config.floatX)
            error_cost = -T.mean(
                (self.input["y_in"] * T.log(out_result + off) + (1 - self.input["y_in"]) * T.log(1 - out_result + off)))
        else:
            out_result = T.nnet.softmax(T.dot(hidden_result, self.wb["w_out"]) + self.wb["b_out"])
            out_result_tag = T.reshape(T.argmax(out_result, axis=1), newshape=(self.input["x_in"].shape[0], 1))
            error_cost = -T.mean(
                T.log(out_result[T.arange(self.input["y_in"].shape[0]), self.input["y_in"][:, 0]] + off))

        error = T.sum(T.cast(T.neq(out_result_tag, self.input["y_in"]), theano.config.floatX)) / \
                self.input["y_in"].shape[0]
        reg_cost = sum(
            [T.sum(T.power(self.wb[k], 2)) for k in filter(lambda s: s[:2] == "w_" and s != "w_embed", self.wb.keys())])
        cost = error_cost + self.p_lambda * 0.5 * reg_cost

        gd = OrderedDict()
        for wb_key in self.wb.keys():
            gd[wb_key] = T.grad(cost=cost, wrt=self.wb[wb_key])

        if self.p_optimizer == "adagrad":
            wb_updates, gradients_sq, deltas_sq = optimizer.adagrad_update(self.wb, gd.values(), self.p_lr, self.p_eps)

        elif self.p_optimizer == "clipped_adagrad":
            wb_updates, gradients_sq, deltas_sq = optimizer.clipped_adagrad_update(self.wb, gd.values(), self.p_lr,
                                                                                   self.p_eps)
        elif self.p_optimizer == "adadelta":
            wb_updates, gradients_sq, deltas_sq = optimizer.adadelta_updates(self.wb, gd.values(), self.p_rho,
                                                                             self.p_eps)
        else:
            wb_updates, gradients_sq, deltas_sq = optimizer.sgd_update(self.wb, gd.values(), self.p_lr)

        self.gradients_sq.update(gradients_sq)
        self.deltas_sq.update(deltas_sq)

        self.func_test = theano.function(
            inputs=[self.input["x_in"]],
            outputs=[x_in_1dim, out_result, out_result_tag],
            allow_input_downcast=True
        )

        self.func_trainer = theano.function(
            inputs=[self.input["x_in"], self.input["y_in"]],
            outputs=[cost, error, error_cost, reg_cost],
            updates=wb_updates,
            allow_input_downcast=True
        )

        self.func_validator = theano.function(
            inputs=[self.input["x_in"], self.input["y_in"]],
            outputs=[cost, error, error_cost, reg_cost],
            allow_input_downcast=True
        )

        self.func_y_seq = theano.function(
            inputs=[self.input["x_in"]],
            outputs=out_result,
            allow_input_downcast=True
        )

    def reset_sq(self):
        params = self.params
        p_eps = self.p_eps
        p_keys = ['gradients_sq', 'deltas_sq']
        for p_key in p_keys:
            for w_key in params[p_key]:
                temp_shape = params[p_key][w_key].get_value().shape
                params[p_key][w_key].set_value((p_eps * np.ones(temp_shape)).astype(theano.config.floatX))


if __name__ == "__main__":
    print 1
