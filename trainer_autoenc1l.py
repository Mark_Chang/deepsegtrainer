import data_generator
import autoencnet_1lsoftmax
import trainer


def trainer_softmax_one():
    s_vocab = 5000
    s_window = 5
    config = {
        "pad_id": s_vocab - 1,
        "s_vocab": s_vocab,
        "s_window": s_window,
        "s_embed": 100,
        "s_hidden": 400,
        "train_file_idx": "./data/converted/as_training_c.utf8",
        "p_lr": 0.1,
        "p_eps": 0.0,
        "p_optimizer": "clipped_adagrad",
        "p_lambda": 0.00001
    }
    mdata_gen = data_generator.SeqDataGenerator(config)
    mmnetwork = autoencnet_1lsoftmax.AutoEncNetwork1LSoftmax(config, mdata_gen)
    mtrainer = trainer.Trainer(mmnetwork)
    model_name = mtrainer.trainig({
        "model_name": "model/as_model_autoenc_1l_full_%s.json",
        "print_time": 10,
        "iter": 50000,
    })




if __name__ == "__main__":
    trainer_softmax_one()
