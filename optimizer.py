import theano.tensor as T
import numpy as np
from collections import OrderedDict
import initializer


def sgd_update(parameters, gradients, eta):
    # create variables to store intermediate updates
    gradients_sq_dict = OrderedDict()
    deltas_sq_dict = OrderedDict()
    deltas = [eta * grad for grad in gradients]
    parameters_updates = [(p, p - d) for p, d in zip(parameters.values(), deltas)]
    return parameters_updates, gradients_sq_dict, deltas_sq_dict


def clipped_adagrad_update(parameters, gradients, eta, eps, clip_val=np.inf):
    # create variables to store intermediate updates
    gradients_sq_dict = OrderedDict()
    deltas_sq_dict = OrderedDict()
    for pkey in parameters.keys():
        pval = parameters[pkey]
        temp_grad = initializer.weight_init_const(pval.get_value().shape, eps)
        gradients_sq_dict.update({pkey: temp_grad})
        # deltas_sq_dict.update({pkey: weight_init_val(temp_delt)})
    # calculates the new "average" delta for the next iteration
    gradients_sq_new = [g_sq + (g ** 2) for g_sq, g in zip(gradients_sq_dict.values(), gradients)]

    # calculates the step in direction. The square root is an approximation to getting the RMS for the average value
    deltas = [T.minimum(eta, (eta / T.sqrt(g_sq + eps))) *grad
              for g_sq, grad in zip(gradients_sq_new, gradients)]

    # Prepare it as a list f
    gradient_sq_updates = zip(gradients_sq_dict.values(), gradients_sq_new)
    # deltas_sq_updates = zip(deltas_sq_dict.values(), deltas_sq_new)
    parameters_updates = [(p, p - d) for p, d in zip(parameters.values(), deltas)]
    return gradient_sq_updates + parameters_updates, gradients_sq_dict, deltas_sq_dict


def adagrad_update(parameters, gradients, eta, eps):
    # create variables to store intermediate updates
    gradients_sq_dict = OrderedDict()
    deltas_sq_dict = OrderedDict()
    for pkey in parameters.keys():
        pval = parameters[pkey]
        temp_grad = initializer.weight_init_const(pval.get_value().shape, eps)

        gradients_sq_dict.update({pkey: temp_grad})
        # deltas_sq_dict.update({pkey: weight_init_val(temp_delt)})
    # calculates the new "average" delta for the next iteration
    gradients_sq_new = [g_sq + (g ** 2) for g_sq, g in zip(gradients_sq_dict.values(), gradients)]

    # calculates the step in direction. The square root is an approximation to getting the RMS for the average value
    deltas = [(eta / T.sqrt(g_sq + eps)) * grad for g_sq, grad in zip(gradients_sq_new, gradients)]

    # Prepare it as a list f
    gradient_sq_updates = zip(gradients_sq_dict.values(), gradients_sq_new)
    # deltas_sq_updates = zip(deltas_sq_dict.values(), deltas_sq_new)
    parameters_updates = [(p, p - d) for p, d in zip(parameters.values(), deltas)]
    return gradient_sq_updates + parameters_updates, gradients_sq_dict, deltas_sq_dict


def adadelta_updates(parameters, gradients, rho, eps):
    # create variables to store intermediate updates
    gradients_sq_dict = OrderedDict()
    deltas_sq_dict = OrderedDict()
    for pkey in parameters.keys():
        pval = parameters[pkey]
        temp_grad = np.zeros(pval.get_value().shape)
        temp_delt = np.zeros(pval.get_value().shape)

        gradients_sq_dict.update({pkey: initializer.weight_init_val(temp_grad)})
        deltas_sq_dict.update({pkey: initializer.weight_init_val(temp_delt)})
    # calculates the new "average" delta for the next iteration
    gradients_sq_new = [rho * g_sq + (1 - rho) * (g ** 2) for g_sq, g in zip(gradients_sq_dict.values(), gradients)]

    # calculates the step in direction. The square root is an approximation to getting the RMS for the average value
    deltas = [(T.sqrt(d_sq + eps) / T.sqrt(g_sq + eps)) * grad for d_sq, g_sq, grad in
              zip(deltas_sq_dict.values(), gradients_sq_new, gradients)]
    # calculates the new "average" deltas for the next step.
    deltas_sq_new = [rho * d_sq + (1 - rho) * (d ** 2) for d_sq, d in zip(deltas_sq_dict.values(), deltas)]

    # Prepare it as a list f
    gradient_sq_updates = zip(gradients_sq_dict.values(), gradients_sq_new)
    deltas_sq_updates = zip(deltas_sq_dict.values(), deltas_sq_new)
    parameters_updates = [(p, p - d) for p, d in zip(parameters.values(), deltas)]
    return gradient_sq_updates + deltas_sq_updates + parameters_updates, gradients_sq_dict, deltas_sq_dict


def l2_regularizer(wb):
    w_array = [T.sum(T.power(wb[k], 2)) for k in filter(lambda s: s[:2] == "w_" and s != "w_embed", wb.keys())]
    # w_array_size = [wb[k].shape[0] * wb[k].shape[1] for k in filter(lambda s: s[:2] == "w_", wb.keys())]
    reg_cost = 0.5 * sum(w_array)  # / T.cast(sum(w_array_size), theano.config.floatX)
    return reg_cost
