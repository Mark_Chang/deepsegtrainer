import data_generator
import autoencnet_embedding

import autoencnet_1lsoftmax
import trainer


def trainer_softmax_one(config, pre_model_name):
    config["s_window"] = 5
    mdata_gen = data_generator.SeqDataGenerator(config)
    mmnetwork = autoencnet_1lsoftmax.AutoEncNetwork1LSoftmax(config, mdata_gen)
    mmnetwork.load_pre_model(pre_model_name)
    mtrainer = trainer.Trainer(mmnetwork)
    mtrainer.trainig({
        "model_name": "model/model_softmax_%s.json",
        "print_time": 10,
        "valid_wait_time": 30,
        "iter": 50000,
    })


def trainer_autoenc_embed(config):
    config["s_window"] = 11
    mdata_gen = data_generator.SegNegSampDataGenerator(config)
    mmnetwork = autoencnet_embedding.AutoEncNetworkEmbedding(config, mdata_gen)
    mtrainer = trainer.Trainer(mmnetwork)
    model_name = ""
    for i, s_negsamp in enumerate([1000, 2000, 3000, 4000]):
        mdata_gen.set_nagsamp_size(s_negsamp)
        model_name = mtrainer.trainig({
            "model_name": "model/model_embed_%s_%s.json" % (i, "%s"),
            "print_time": 10,
            "valid_wait_time": 50,
            "iter": 1000,
        })
        mmnetwork.reset_sq()
    return model_name


def train_autoenc_all():
    s_vocab = 5000
    config = {
        "pad_id": s_vocab - 1,
        "s_vocab": s_vocab,
        "s_embed": 100,
        "s_e_hidden": 400,
        "s_hidden": 400,
        "s_hidden2": 400,
        "s_hidden3": 400,
        "train_file_idx": "./data/converted/as_training_c.utf8",
        #"valid_file_idx": "./data/converted/as_valid_c.utf8",
        "p_lr": 0.1,
        "p_eps": 0.0,
        "p_optimizer": "clipped_adagrad",
        "p_lambda": 0.00001
    }
    model_name = trainer_autoenc_embed(config)
    trainer_softmax_one(config, model_name)


if __name__ == "__main__":
    train_autoenc_all()
