import random
from util import read_file_line, read_json,gen_word_idx, gen_word_label
random.seed(1)


# U 0 unigram
# B 1 begin
# L 2 last
# I 3 intermediate





def gen_training_file_label(train_file, train_file_idx, word_dict):
    # f = open(train_file,"r")
    f2 = open(train_file_idx, "w")
    for line in read_file_line(train_file, True):
        line_write = gen_word_label(line.decode("utf-8"), word_dict)
        if len(line_write) > 1:
            f2.write("%s\n" % (" ".join([str(x) for x in line_write])))
    f2.close()


def gen_training_file_idx(train_file, train_file_idx, word_dict):
    # f = open(train_file,"r")
    f2 = open(train_file_idx, "w")
    for line in read_file_line(train_file, True):
        line_write = gen_word_idx(line.decode("utf-8"), word_dict)
        if len(line_write) > 0:
            f2.write("%s\n" % (" ".join([str(x) for x in line_write])))
    f2.close()


def gen_train_file_label(ifile, ofile, word_dict):
    # f = open(train_file,"r")
    f_t_label = open(ofile , "w")
    for line in read_file_line(ifile, True):
        line_write_label = gen_word_label(line.decode("utf-8"), word_dict)
        line_write_idx = gen_word_idx(line.decode("utf-8"), word_dict)
        if len(line_write_label) > 0 and len(line_write_idx) > 0:
                f_t_label.write("%s\n" % (" ".join([str(x) for x in line_write_label])))
    f_t_label.close()


def main():

    dict_fname = "as_idx_dict.json"
    word_dict = read_json(dict_fname)
    ifile = "./data/icwb2-data/training/as_training.utf8"
    ofile = "./data/converted/as_training_c.utf8"
    gen_train_file_label(ifile, ofile, word_dict)



if __name__ == "__main__":
    main()
